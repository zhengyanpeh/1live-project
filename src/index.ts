import Koa = require('koa');
import Router = require('koa-router');
import dotenv = require('dotenv');
import * as fs from 'fs'
import { RMSDK } from 'rm-api-sdk'

import { oauthFn, oauthCb } from './oauth'

dotenv.config();
const app = new Koa();
const router = new Router();

router
  .get('/oauth', oauthFn())
  .get('/oauth/callback', oauthCb())

app
  .use(async (ctx, next) => {
    ctx.RMSDK = RMSDK({
      clientId: process.env.ClientId || '',
      clientSecret: process.env.ClientSecret || '', 
      privateKey: Buffer.from(fs.readFileSync('privateKey.pem')).toString(),
    })
    await next()
  })
  .use(router.routes())
  .use(router.allowedMethods())
  .use(ctx => ctx.status = 404)

;(async () => {
    console.log('Listening on port 3000')
    await app.listen(3000);
})();
