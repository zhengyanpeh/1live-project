import { Context } from 'koa'

export const oauthFn = () => async (ctx: Context) => {
    const resp = await ctx.RMSDK.getClientCredentials();

    ctx.status = 200
    ctx.body = resp
}

export const oauthCb = () => async (ctx: Context) => {
    const resp = await ctx.RMSDK.getClientCredentials();

    ctx.status = 200
    ctx.body = resp
}
